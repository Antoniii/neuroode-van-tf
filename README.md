![](https://gitlab.com/Antoniii/neuroode-van-tf/-/raw/main/2b33a741268cb6ef2ed0aaa8c9307a6b.png)

![](https://gitlab.com/Antoniii/neuroode-van-tf/-/raw/main/Кривая_прогноза_и_фактическая_кривая.png)

## Sources

* [Oчень простая нейронная сеть для решения обыкновенных дифференциальных уравнений и уравнений в частных производных](https://russianblogs.com/article/5302880711/)
* [Применение методов машинного обучения для решения дифференциальных уравнений](https://dspace.spbu.ru/bitstream/11701/32448/1/VKR_final__1_.pdf)
