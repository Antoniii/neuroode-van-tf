# import tensorflow as tf
import tensorflow.compat.v1 as tf
import matplotlib.pyplot as plt
import numpy as np

x_train = np.linspace (0,2,10, endpoint = True) # Генерировать 100 точек в интервале [0,2]
y_trail = np.exp (-0.5 * x_train ** 2) / (1 + x_train + x_train ** 3) + x_train ** 2 # Известные аналитические решения для сравнения
x_t = np.zeros((len(x_train),1))

for i in range(len(x_train)):
    x_t[i] = x_train[i]

tf.disable_v2_behavior()
x1 = tf.placeholder("float", [None, 1]) # Передавать 100 точек за раз [100,1]

W = tf.Variable(tf.zeros([1, 10]))
b = tf.Variable(tf.zeros([10]))
y1 = tf.nn.sigmoid (tf.matmul (x1, W) + b) # Форма функции активации сигмоида y1 [100,10]
W1 = tf.Variable(tf.zeros([10, 1]))
b1 = tf.Variable(tf.zeros([1]))
y = tf.matmul(y1, W1) # Выход сети [100,1]

lq = (1 + 3*(x1**2))/(1 + x1 + x1**3)
dif = tf.matmul(tf.multiply(y1*(1 - y1), W), W1) # dy / dx, dif shape [100,1], который является значением производной соответствующей точки
t_loss = (dif + (x1 + lq)*y - x1**3 - 2*x1 - lq*x1**2)**2 # квадрат обыкновенного дифференциального уравнения F

loss = tf.reduce_mean (t_loss) + (y[0] - 1)**2 # После суммирования точек F-квадрата каждой точки, возьмите среднее значение и добавьте граничные условия

train_step = tf.train.AdamOptimizer(1e-2).minimize(loss) # Параметры обучающей сети оптимизатора Адама
init = tf.global_variables_initializer()
sess = tf.InteractiveSession()
sess.run(init)

for i in range(50000): #training 50000 раз
    sess.run(train_step,feed_dict={x1: x_t})
    if i%50 == 0:
        total_loss = sess.run(loss,feed_dict={x1: x_t})
        print("loss={}".format(total_loss))
        print(sess.run(y[0], feed_dict={x1: x_t}))

saver = tf.train.Saver(max_to_keep = 1) # Сохраняем модель, после однократного обучения можно закомментировать процесс обучения
saver.save(sess,'ckpt/nn.ckpt',global_step=50000)
saver = tf.train.Saver(max_to_keep=1)
model_file="ckpt/nn.ckpt-50000"
saver.restore(sess, model_file)

output = sess.run(y,feed_dict={x1:x_t})
output1 = sess.run(t_loss,feed_dict={x1:x_t})
y_output = x_train.copy()
y_output1 = x_train.copy()

for i in range(len(x_train)):
    y_output[i] = output[i]
    y_output1[i] = output1[i]

fig = plt.figure("Кривая прогноза и фактическая кривая")
plt.plot(x_train,y_trail)
plt.plot(x_train,y_output)
fig2 = plt.figure ("y_-y") # Рисуем отклонение между фактическим и прогнозируемым значением
plt.plot(x_train,y_trail-y_output)
fig3 = plt.figure ("loss") # Нарисуйте вклад каждой точки в Loss
plt.plot(x_train,y_output1+(y_output[0]-1)**2)
plt.show()